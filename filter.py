import pandas as pd
import numpy as np
import csv
from dataclasses import dataclass

###########################################
    ###DEFINING CLASSES AND FUNCTIONS###


"""
A Dataclass to facilitate convenient handling of Eddypro ouput files
"""
@dataclass
class ep_output:
    df: pd.DataFrame
    metadata: []
    units: []
    colnames: []
    
"""
Opens biomet or full ouputfiles as ep_output dataclass
"""
def open_eddypro_output_file(file_path, filetype):

    if filetype == "full":
        # Variables to hold your metadata, units, and column names
        metadata = []
        units = []
        colnames = []

               # Open files
        with open(file_path, 'r') as file:
            metadata = file.readline().strip()  # Read the first line for metadata
            colnames = file.readline().strip().split(',')  # Read the second line for column names
            units = file.readline().strip()  # Read the third line for units

        df = pd.read_csv(file_path,skiprows=3,names=colnames)

        data = ep_output(df,metadata,units,colnames)

    if filetype == "biomet":
        units = []
        colnames = []

        with open(file_path, 'r') as file:
            colnames = file.readline().strip().split(',')  # Read the first line for column names
            units = file.readline().strip()  # Read the second line for full_units

        df = pd.read_csv(file_path,skiprows=2,names=colnames)
        data = ep_output(df,[],units,colnames)
    else:
        data = ep_output(pd.DataFrame(),[],[],[])

    return data

"""
Filters negative co2 fluxes which occure at night(SWIN <10)
"""
def filter_neg_flux_night(full_df,biomet_df):
    ###### FILTER STEP 1
    # Filtering full output based on biomet data without altering dataframe structure

    # Step 1: Combine date and time into a single datetime column for both DataFrames
    full_df['datetime'] = pd.to_datetime(biomet_df['date'] + ' ' + biomet_df['time'])
    biomet_df['datetime'] = pd.to_datetime(full_df['date'] + ' ' + full_df['time'])

    # Step 2: Set the datetime column as the index
    full_df.set_index('datetime', inplace=True)
    biomet_df.set_index('datetime', inplace=True)

    # Step 3 & 4: Iterate through biomet_df and apply conditions to full_df at the same datetime index

    # When SWIN is too low (night time) remove negative co2 fluxes
    # When its raining, remove co2 fluxes in general
    for dt, row in biomet_df.iterrows():
        if row['SWIN_1_1_1'] < 10:
            if dt in full_df.index and full_df.at[dt,"co2_flux"] <0:
                full_df.at[dt, 'co2_flux'] = np.nan

    # Reset and drop index column
    biomet_df.reset_index(inplace=True)
    full_df.reset_index(inplace=True)
    biomet_df.drop('datetime', axis=1, inplace=True)
    full_df.drop('datetime', axis=1, inplace=True)

    return full_df

"""
Filters co2 flux while raining
"""
def filter_flux_rain(full_df,biomet_df):
    ###### FILTER STEP 1
    # Filtering full output based on biomet data without altering dataframe structure
    
    # Step 1: Combine date and time into a single datetime column for both DataFrames
    full_df['datetime'] = pd.to_datetime(biomet_df['date'] + ' ' + biomet_df['time'])
    biomet_df['datetime'] = pd.to_datetime(full_df['date'] + ' ' + full_df['time'])
    
    # Step 2: Set the datetime column as the index
    full_df.set_index('datetime', inplace=True)
    biomet_df.set_index('datetime', inplace=True)
    
    # Step 3 & 4: Iterate through biomet_df and apply conditions to full_df at the same datetime index
    
    # When SWIN is too low (night time) remove negative co2 fluxes
    # When its raining, remove co2 fluxes in general
    for dt, row in biomet_df.iterrows():
        if row["P_RAIN_1_1_1"] >0:
            if dt in full_df.index:
                full_df.at[dt, "co2_flux"] = np.nan
    
    # Reset and drop index column
    biomet_df.reset_index(inplace=True)
    full_df.reset_index(inplace=True)
    biomet_df.drop('datetime', axis=1, inplace=True)
    full_df.drop('datetime', axis=1, inplace=True)

    return full_df

"""
Filters values for given columns which exeed +/- 3*standard deviation
"""
def filter_std_deviation(full_df,columns_to_process):
    for column in columns_to_process:
        # Remove already present -9999 values
        full_df[column] = full_df[column].replace(-9999, np.nan)
    
        # Calculate mean and standard deviation
        mean = full_df[column].mean()
        std_dev = full_df[column].std()
        
        # Identify outliers and create a mask
        outliers_mask = np.abs(full_df[column] - mean) > (3 *std_dev)
        
        # Replace outliers with -9999
        full_df.loc[outliers_mask, column] = np.nan

    return full_df

"""
Exports ep_ouput dataclass into eddypro resembling csv file
"""
def export_eddypro_format(data):
        # replace na values with conventional value
        data.df = data.df.fillna(-9999)
        # Cast every column to str so that we can replace -9999.0 (due to float casting) with "-9999" for the correct value
        data.df = data.df.astype(str)
        data.df = data.df.replace("-9999.0","-9999")

        # Write the full_metadata, column names, and full_units back, then append the modified DataFrame
        with open('modified_file.csv', 'w', newline='') as file:
            file.write(data.metadata + '\n')  # Write the full_metadata
            file.write(','.join(data.colnames) + '\n')  # Write the column names as a comma-separated string
            file.write(data.units + '\n')  # Write the full_units

        # Append the modified DataFrame to the CSV, without including the index and header
        data.df.to_csv('modified_file.csv', mode='a', index=False, header=False)

###########################################
        ###FUNCTION CALLS###


# Path to your CSV files
full_file_path = "data\eddypro_ecov_march_dec_full_output_2024-01-11T105215_adv.csv"
biomet_file_path = "data\eddypro_ecov_march_dec_biomet_2024-01-11T105215_adv.csv"

# 
full_data = open_eddypro_output_file(full_file_path,"full")
biomet_data = open_eddypro_output_file(biomet_file_path,"biomet")

full_data.df = filter_neg_flux_night(full_data.df,biomet_data.df)
full_data.df = filter_flux_rain(full_data.df,biomet_data.df)


full_data.df = filter_std_deviation(full_data.df,['H', 'LE', 'co2_flux'])

export_eddypro_format(full_data)